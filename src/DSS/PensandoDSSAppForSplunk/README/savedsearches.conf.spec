[<stanza name>]
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.showLabels = <string>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.showLegend = <string>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.showTooltip = <string>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.useColors = <string>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.colorMode = <string>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.minColor = <string>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.maxColor = <string>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.numOfBins = <float>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.showSelf = <string>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.showBackwards = <string>
display.visualizations.custom.PensandoDSSAppForSplunk.sankey_diagram.styleBackwards = <string>