# Pensando Add-on for Splunk


## OVERVIEW
The "Pensando Add-on for Splunk" parses the firewall logs collected from the Pensando DSC and DSS platforms. To visualize DSC data in Splunk dashboards, please install "Pensando App for Splunk" and to visualize DSS data in Splunk dashboards, please install "Pensando DSS App for Splunk".

* Author - Pensando
* Version - 1.1.0
* Build - 5


## COMPATIBILITY MATRIX
* Browser: Chrome, Safari, and Firefox
* OS: Platform Independent
* Splunk Enterprise version: 8.0.X, 8.1.X, 8.2.X
* Supported Splunk Deployment: Splunk Cluster, Splunk Standalone, and Distributed Deployment


## RELEASE NOTES

### Version 1.1.0
* Added support to parse Syslog data for the DSS platform.
* Added CIM mapping for DSS Syslog data.


## RECOMMENDED SYSTEM CONFIGURATION
* Standard Splunk configuration


## TOPOLOGY AND SETTING UP SPLUNK ENVIRONMENT
* This app has been distributed in three parts.
    
    1. **Pensando Add-on for Splunk**, which parses the firewall logs collected from Pensando DSC and DSS platforms.
    2. **Pensando App for Splunk**, which adds dashboards to visualize the collected data for the DSC platform.
    3. **Pensando DSS App for Splunk**, which adds dashboards to visualize the collected data for the DSS platform.

* This app can be set up in two ways:
    
**1) Standalone Mode**:

* Install either "Pensando App for Splunk" or "Pensando DSS App for Splunk" and "Pensando Add-on for Splunk" on a single machine. This single machine would serve as a Search Head + Indexer + Heavy Forwarder for this setup.
* The "Pensando App for Splunk" uses the DSC data parsed by "Pensando Add-on for Splunk" and builds dashboards on it.
* The "Pensando DSS App for Splunk" uses the DSS data parsed by "Pensando Add-on for Splunk" and builds dashboards on it.

  
**2) Distributed Environment**:

a) With heavy forwarder:

* Install either "Pensando App for Splunk" or "Pensando DSS App for Splunk" and "Pensando Add-on for Splunk" on the search head.   
* Add-on resides on Search Head machine need not require any configuration here.
* Add-on needs to be installed and configured on the Heavy Forwarder system.
* User needs to create data input on Splunk Heavy Forwarder to collect data from Pensando.
* User needs to manually create an index on the Indexer (No need to install "Pensando App for Splunk" or "Pensando DSS App for Splunk" or "Pensando Add-on for Splunk" on Indexer).

b) With Splunk Universal Forwarder

* Install either "Pensando App for Splunk" or "Pensando DSS App for Splunk" and "Pensando Add-on for Splunk" on the search head.
* Add-on resides on Search Head machine need not require any configuration here.
* Add-on needs to be installed and configured on the Universal Forwarder system.
* User needs to create data input on Universal Forwarder to collect data from Pensando.
* Add-on needs to be installed on the Indexer if Add-on is collecting DSS Syslog data.
* User needs to manually create an index on the Indexer. (No need to install "Pensando App for Splunk" or "Pensando DSS App for Splunk" on Indexer. If the user wants to collect DSS Syslog data then the "Pensando Add-on for Splunk" Add-on should be installed on Indexer.)


## INSTALLATION
"Pensando Add-on for Splunk" can be installed through UI using "Manage Apps" > "Install app from file" or by extracting tarball directly into $SPLUNK_HOME/etc/apps/ folder.


## CONFIGURATION

### Configure Inputs on Splunk Forwarder Instance:

The "Pensando Add-on for Splunk" manages inputs through TCP/UDP inputs provided by Splunk. To configure inputs:

* Login to Splunk WEB UI.
* Navigate to “Settings > Data inputs.
* Choose TCP or UDP and click New.
* In the left pane, click TCP / UDP to add an input.
* Click the TCP or UDP button to choose between a TCP or UDP input.
* In the Port field, enter a port number on which you are forwarding the logs from Pensando DSC or DSS device.
* In the Source name override field, enter a new source name to override the default source value, if necessary.
* Click Next to continue to the Input Settings page.
* Set the appropriate Source type:
    *  if the device is DSC then select “pensando:dsc” sourcetype.
    *  if the device is DSS then select “pensando:dss” sourcetype.
* Set App context to “TA-Pensando”.
* Set the Host to either IP or DNS. This value will be reflected in the host field of the events. This should be the name of the machine from which the event originates.
* Set the Index that Splunk Enterprise should send data to for this input.
* Click Review.
* Click Submit once you have ensured everything is correct.

Once the input is configured, execute the following queries to validate that the events are being received.

* If the device is DSC then execute following query:
    * index=<configured_index> sourcetype="pensando:dsc" 
  
* If the device is DSS then execute following query:
    * index=<configured_index> sourcetype="pensando:dsc" 

**NOTE:**

* If the device is DSS and the user configures the UDP input then Splunk appends a timestamp and connected host(or IP) to the UDP header data and strips the syslog_priority. This behavior is not causing any problem in the Add-on and App. If the user needs the same UDP header data in Splunk then follow the steps mentioned in the TROUBLESHOOTING section.

### Configure Event Types on Splunk Search Head Instance:

To use the CIM mapped fields, user first needs to configure the event type to provide the index in which the data is being collected. To configure event type:

* Navigate to Settings > Event types.
* Select “Pensando Add-on for Splunk” from the App dropdown.
* Click on pensando_idx.
* Update “index=main” with “index=<your_configured_index>” in the existing definition to use your configured index.
* Click Save.


## UPGRADE

### From v1.0.0 to v1.1.0
* No manual steps are required to upgrade the "Pensando Add-on for Splunk" from version 1.0.0 to version 1.1.0

**NOTE:** In Splunk distributed environment with Universal Forwarder, if User wants to collect DSS Syslog data then "Pensando Add-on for Splunk" Add-on should be installed on Indexer.


## TROUBLESHOOTING

* To check the fields extracted by the TA for the DSC device then execute query:
    *  `index=<your_index_name> sourcetype="pensando:dsc"` in Splunk in verbose mode.

* To check the fields extracted by the TA for the DSS device then execute query:
    *  `index=<your_index_name> sourcetype="pensando:dss"` in Splunk in verbose mode.

* If the device is DSS and the user configures the UDP input then Splunk appends a timestamp and connected host(or IP) to the UDP header data and strips the syslog_priority. To get the same UDP header data follow the below steps:
    * Configure the UDP input as mentioned in the CONFIGURATION section.
    * Edit the inputs.conf file located at $SPLUNK_HOME/etc/apps/TA-Pensando/local/
    * Add the following two properties under your configured stanza.
        ```
        [udp://<your_configured_port_number>]
        no_appending_timestamp = true
        no_priority_stripping = true
        ```
    * Save the file.
    * Restart the Splunk.
  
**NOTE:**

* Make sure that the user enables the forwarding on a configured port from the DSS device after performing the above steps.
* $SPLUNK_HOME denotes the path where Splunk is installed. Ex: /opt/splunk


## UNINSTALL & CLEANUP STEPS

* Remove $SPLUNK_HOME/etc/apps/TA-Pensando/
* To reflect the cleanup changes in UI, Restart Splunk Enterprise instance


## SUPPORT
* Support Offered: Yes
* Support Email: splunkapp@pensando.io

### Copyright © 2022 Pensando. All rights reserved.