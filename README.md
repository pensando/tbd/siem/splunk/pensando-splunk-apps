# Pensando Splunk

The current version of the apps support the DSS Ajax release fully.  They *can* be used for releases through Captain Marvel, but some of the fields towards the end of the syslog (policy_name and ui_policy_name) don't show up in the visualizations as of yet.

To install, clone this repo and use the .spl files in the DSS directory and follow the directions in the PDF file in the same directory.

The TA is not currently on Splunkbase.  Splunk deleted it and we are going through the verification and approval processes again. When that is done, we will update the README to reflect that.  For now, you must get the .spl files and install them that way and configure via the directions in the PDF file.
